# swe-data for debian

.PHONY: all clean mrproper install packages dist \
	download-basic-data download-standard-data download-extra-data download-sat-data \
	install-basic-data install-standard-data install-extra-data install-sat-data

# for ../swe-data_VERSION.orig.tar.gz
VERSION = 4.0-20230403
URL = https://www.astro.com/ftp/swisseph
DESTDIR = /usr/local
EPHEPATH = $(DESTDIR)/share/libswe/ephe

WGET = wget
WGETOP = --no-directories --no-host-directories --timestamping --no-remove-listing

BSC_DIR = swe-basic-data
STD_DIR = swe-standard-data
EXT_DIR = swe-extra-data
SAT_DIR = swe-sat-data

DIRS = $(BSC_DIR) $(STD_DIR) $(EXT_DIR) $(SAT_DIR) $(EPHEPATH) $(EPHEPATH)/sat

.DEFAULT_GOAL = all

$(DIRS):
	mkdir -p $@

BSC_NAMES = sepl_18.se1 semo_18.se1 seas_18.se1 \
			seasnam.txt seasnam2.txt seorbel.txt \
			sefstars.txt
BSC_PATHS = $(addprefix $(BSC_DIR)/, $(BSC_NAMES))

$(BSC_PATHS): $(BSC_DIR)
	cd $< && $(WGET) $(WGETOP) $(URL)/ephe/$(notdir $@)

BSC_OTHER = $(BSC_DIR)/seleapsec.txt $(BSC_DIR)/sedeltat.txt.inactive

$(BSC_OTHER): $(BSC_DIR)
	cd $< && $(WGET) $(WGETOP) $(URL)/src/$(notdir $@)

download-basic-data: $(BSC_PATHS) $(BSC_OTHER)

install-basic-data: $(EPHEPATH) $(BSC_PATHS) $(BSC_OTHER)
	cp -f $(BSC_PATHS) $(BSC_OTHER) $<

STD_NAMES = seplm54.se1 semom54.se1 seasm54.se1 \
		    seplm48.se1 semom48.se1 seasm48.se1 \
		    seplm42.se1 semom42.se1 seasm42.se1 \
		    seplm36.se1 semom36.se1 seasm36.se1 \
		    seplm30.se1 semom30.se1 seasm30.se1 \
		    seplm24.se1 semom24.se1 seasm24.se1 \
		    seplm18.se1 semom18.se1 seasm18.se1 \
		    seplm12.se1 semom12.se1 seasm12.se1 \
			seplm06.se1 semom06.se1 seasm06.se1 \
			sepl_00.se1 semo_00.se1 seas_00.se1 \
			sepl_06.se1 semo_06.se1 seas_06.se1 \
			sepl_12.se1 semo_12.se1 seas_12.se1 \
			sepl_24.se1 semo_24.se1 seas_24.se1 \
			sepl_30.se1 semo_30.se1 seas_30.se1 \
		    sepl_36.se1 semo_36.se1 seas_36.se1 \
		    sepl_42.se1 semo_42.se1 seas_42.se1 \
		    sepl_48.se1 semo_48.se1 seas_48.se1
STD_PATHS = $(addprefix $(STD_DIR)/, $(STD_NAMES))

$(STD_PATHS): $(STD_DIR)
	cd $< && $(WGET) $(WGETOP) $(URL)/ephe/$(notdir $@)

download-standard-data: $(STD_PATHS)

install-standard-data: $(EPHEPATH) $(STD_PATHS)
	cp -f $(STD_PATHS) $<

EXT_NAMES = seplm132.se1 semom132.se1 seasm132.se1 \
			seplm126.se1 semom126.se1 seasm126.se1 \
			seplm120.se1 semom120.se1 seasm120.se1 \
			seplm114.se1 semom114.se1 seasm114.se1 \
			seplm108.se1 semom108.se1 seasm108.se1 \
			seplm102.se1 semom102.se1 seasm102.se1 \
			seplm96.se1 semom96.se1 seasm96.se1 \
			seplm90.se1 semom90.se1 seasm90.se1 \
			seplm84.se1 semom84.se1 seasm84.se1 \
			seplm78.se1 semom78.se1 seasm78.se1 \
			seplm72.se1 semom72.se1 seasm72.se1 \
			seplm66.se1 semom66.se1 seasm66.se1 \
			seplm60.se1 semom60.se1 seasm60.se1 \
			sepl_54.se1 semo_54.se1 seas_54.se1 \
			sepl_60.se1 semo_60.se1 seas_60.se1 \
			sepl_66.se1 semo_66.se1 seas_66.se1 \
			sepl_72.se1 semo_72.se1 seas_72.se1 \
			sepl_78.se1 semo_78.se1 seas_78.se1 \
			sepl_84.se1 semo_84.se1 seas_84.se1 \
			sepl_90.se1 semo_90.se1 seas_90.se1 \
			sepl_96.se1 semo_96.se1 seas_96.se1 \
			sepl_102.se1 semo_102.se1 seas_102.se1 \
			sepl_108.se1 semo_108.se1 seas_108.se1 \
			sepl_114.se1 semo_114.se1 seas_114.se1 \
			sepl_120.se1 semo_120.se1 seas_120.se1 \
			sepl_126.se1 semo_126.se1 seas_126.se1 \
			sepl_132.se1 semo_132.se1 seas_132.se1 \
			sepl_138.se1 semo_138.se1 seas_138.se1 \
			sepl_144.se1 semo_144.se1 seas_144.se1 \
			sepl_150.se1 semo_150.se1 seas_150.se1 \
			sepl_156.se1 semo_156.se1 seas_156.se1 \
			sepl_162.se1 semo_162.se1 seas_162.se1
EXT_PATHS = $(addprefix $(EXT_DIR)/, $(EXT_NAMES))

$(EXT_PATHS): $(EXT_DIR)
	cd $< && $(WGET) $(WGETOP) $(URL)/ephe/$(notdir $@)

download-extra-data: $(EXT_PATHS)

install-extra-data: $(EPHEPATH) $(EXT_PATHS)
	cp -f $(EXT_PATHS) $<

SAT_NAMES = sepm9401.se1 \
			sepm9402.se1 \
			sepm9501.se1 \
			sepm9502.se1 \
			sepm9503.se1 \
			sepm9504.se1 \
			sepm9599.se1 \
			sepm9601.se1 \
			sepm9602.se1 \
			sepm9603.se1 \
			sepm9604.se1 \
			sepm9605.se1 \
			sepm9606.se1 \
			sepm9607.se1 \
			sepm9608.se1 \
			sepm9699.se1 \
			sepm9701.se1 \
			sepm9702.se1 \
			sepm9703.se1 \
			sepm9704.se1 \
			sepm9705.se1 \
			sepm9799.se1 \
			sepm9801.se1 \
			sepm9802.se1 \
			sepm9808.se1 \
			sepm9899.se1 \
			sepm9901.se1 \
			sepm9902.se1 \
			sepm9903.se1 \
			sepm9904.se1 \
			sepm9905.se1 \
			sepm9999.se1
SAT_PATHS = $(addprefix $(SAT_DIR)/, $(SAT_NAMES))

$(SAT_PATHS): $(SAT_DIR)
	cd $< && $(WGET) $(WGETOP) $(URL)/ephe/sat/$(notdir $@)

download-sat-data: $(SAT_PATHS)

install-sat-data: $(EPHEPATH)/sat $(SAT_PATHS)
	cp -f $(SAT_PATHS) $<

ALL = $(BSC_PATHS) $(BSC_OTHER) $(STD_PATHS) $(EXT_PATHS) $(SAT_PATHS)

all: $(ALL)

clean:

mrproper:
	rm -rf $(BSC_DIR) $(STD_DIR) $(EXT_DIR) $(SAT_DIR)

install: install-basic-data install-standard-data install-extra-data \
	install-sat-data

packages:
	dpkg-buildpackage -us -uc

../swe-data_$(VERSION).orig.tar.gz: $(ALL)
	mkdir swe-data-$(VERSION)
	cp -fP Makefile README.txt swe-data-$(VERSION)
	cp -rf swe-*-data swe-data-$(VERSION)
	tar czf $@ swe-data-$(VERSION)
	rm -rf swe-data-$(VERSION)

dist: ../swe-data_$(VERSION).orig.tar.gz

