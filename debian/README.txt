swe-data for Debian
-------------------

Packaging Astrodienst' ephemeris files and static data.

The files are individually distributed from https://www.astro.com/ftp/swisseph

Source of packages:

 - swe-basic-data
 - swe-standard-data
 - swe-extra-data
 - swe-sat-data
 - swe-data (meta-package)

.
